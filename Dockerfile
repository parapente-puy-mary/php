FROM php:7.4-fpm-alpine

# Use the default production configuration
RUN cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Package dependencies
RUN apk add --no-cache git icu-dev zlib-dev libpng-dev jpeg-dev libzip-dev && \
  docker-php-ext-install gd intl pdo_mysql zip
